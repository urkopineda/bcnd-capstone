const Verifier = artifacts.require('./Verifier.sol')
const SolnSquareVerifier = artifacts.require('./SolnSquareVerifier.sol')

module.exports = (deployer) => {
  deployer.deploy(Verifier)
    .then(() => deployer.deploy(SolnSquareVerifier, Verifier.address))
}
