const verifier = artifacts.require('Verifier')
const proof = require('../zokrates/code/square/proof')

contract('TestSquareVerifier', (accounts) => {

  const account = accounts[0]

  describe('Test verification', () => {
    beforeEach(async () => {
      try {
        this.contract = await verifier.new({from: account})
      } catch(error) {
        console.log(error)
      }
    })

    it('verification with correct proof', async () => {
      const result = await this.contract.verifyTx.call(
        proof.proof.A, 
        proof.proof.A_p, 
        proof.proof.B, 
        proof.proof.B_p, 
        proof.proof.C, 
        proof.proof.C_p, 
        proof.proof.H, 
        proof.proof.K, 
        proof.input,
        {
          from: account
        }
      )
      assert.equal(result, true, 'Proof is not Verified') 
    })

    it('verification with incorrect proof', async () => { 
      const proofB_p = [
        '0x19117a9d32a3e757322666bbebe7741b28701e409132e79aa005877d9d0611c5',
        '0x08925b7e79d379b941458031d4df0f06404235637f00f6dba0af54bbb5c53a71'
      ]
      const result = await this.contract.verifyTx.call(
        proof.proof.A, 
        proof.proof.A_p, 
        proof.proof.B, 
        proofB_p, 
        proof.proof.C, 
        proof.proof.C_p, 
        proof.proof.H, 
        proof.proof.K, 
        proof.input,
        {
          from: account
        }
      )
      assert.equal(result, false, 'The proof is in correct. Should not verify')
    })
  })
})
