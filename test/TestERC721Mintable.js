const verifier = artifacts.require('Verifier')
const ERC721MintableComplete = artifacts.require('CustomERC721Token')

contract('Test "ERC721Mintable"...', accounts => {
  const contractURI = 'https://s3-us-west-2.amazonaws.com/udacity-blockchain/capstone/1'
  const token = 1
  const account1 = accounts[0]
  const account2 = accounts[1]
  const account3 = accounts[2]

  describe('Match ERC721 specs...', () => {
    beforeEach(async () => { 
      const verifierContract = await verifier.new({
        from: account1
      })
      this.contract = await ERC721MintableComplete.new(verifierContract.address, {
        from: account1
      })
      await this.contract.mint(account2, token, {
        from: account1
      })
    })

    it('should return total supply', async () => {
      const tokens = await this.contract.totalSupply({
        from: account1
      })
      assert.equal(tokens, token, 'Total tokens supply incorrect')
    })

    it('should get token balance', async () => {
      const tokens = await this.contract.balanceOf(account2, {
        from: account2
      })
      assert.equal(tokens, token, 'Number of tokens do not match')
    })

    it('should return token uri', async () => { 
      const uri = await this.contract.tokenURI(token)
      assert.equal(contractURI, uri, 'URIs do not match')
    })

    it('should transfer token from one owner to another', async () => { 
      await this.contract.transferFrom(account2, account3, token, {
        from: account2
      })
      const newOwner = await this.contract.ownerOf(token, {
        from: account3
      })
      assert.equal(newOwner, account3, 'Transfer not sucessful')
    })
  })

  describe('have ownership properties', () => {
    beforeEach(async () => { 
      this.contract = await ERC721MintableComplete.new({
        from: account1
      })
    })

    it('should fail when minting when address is not contract owner', async () => { 
      let revert = false
      try {
        await this.contract.mint(
          account2, 
          token,
          {
            from: account2
          }
        )
      } catch (error) {
        revert = true
      }
      assert.equal(revert, true, 'Unable to mint because is not the owner')
    })

    it('should return contract owner', async () => { 
      const owner = await this.contract.getOwnerAddress({
        from: account2
      })
      assert.equal(owner, account1, 'Account is not the owner')
    })
  })

})