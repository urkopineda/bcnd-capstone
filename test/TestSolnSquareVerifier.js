const verifier = artifacts.require('Verifier')
const solnSquareVerifier = artifacts.require('SolnSquareVerifier')
const proof = require('../zokrates/code/square/proof')

contract('Test "SolSquareVerifier"...', (accounts) => {
  const token = 1
  const account1 = accounts[0]
  const account2 = accounts[1]

  describe('Test verification system...', () => {
    beforeEach(async () => { 
      const verifierContract = await verifier.new({
        from: account1
      })
      this.contract = await solnSquareVerifier.new(verifierContract.address, {
        from: account1
      })
    })

    it('successfully mint token', async () => {
      let revert = false
      try {
        await this.contract.mintVerified(
          account2, 
          token,
          proof.proof.A, 
          proof.proof.A_p, 
          proof.proof.B, 
          proof.proof.B_p, 
          proof.proof.C, 
          proof.proof.C_p, 
          proof.proof.H, 
          proof.proof.K, 
          proof.input,
          {
            from: account1
          }
        )
      } catch (error) {
        revert = true
      }
      assert.equal(revert, false, 'Unable to mint token') 
    })

    it('mint token with same solution', async () => { 
      await this.contract.mintVerified(
        account2, 
        token,
        proof.proof.A, 
        proof.proof.A_p, 
        proof.proof.B, 
        proof.proof.B_p, 
        proof.proof.C, 
        proof.proof.C_p, 
        proof.proof.H, 
        proof.proof.K, 
        proof.input,
        {
          from: account1
        }
      )
      let revert = false
      try {
        await this.contract.mintVerified(
          account2, 
          token,
          proof.proof.A, 
          proof.proof.A_p, 
          proof.proof.B, 
          proof.proof.B_p, 
          proof.proof.C, 
          proof.proof.C_p, 
          proof.proof.H, 
          proof.proof.K, 
          proof.input,
          {
            from: account1
          }
        )
      } catch (error) {
        revert = true
      }
      assert.equal(revert, true, 'Minting with the same solution not allowed')
    })
  })
}) 
