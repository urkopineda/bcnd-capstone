const HDWalletProvider =  require('truffle-hdwallet-provider')
require('dotenv').config()

module.exports = {
  networks: {
    development: {
      host: '127.0.0.1',
      port: 8545,
      network_id: '*'
    },
    rinkeby: {
      provider: () => new HDWalletProvider(process.env.MNEMONIC, `https://rinkeby.infura.io/v3/${process.env.INFURA_API_KEY}`),
      network_id: '4',
      gas: 4500000,
      gasPrice: 10000000000
    }
  },
  mocha: {},
  compilers: {
    solc: {
      version: '0.5.2'
    }
  }
}
