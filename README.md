# Capstone: Real Estate Marketplace

The capstone will build upon the knowledge you have gained in the course in order to build a decentralized housing product.

> NOTE: There are some screenshots with the test working in the `./screenshots` directory:
> * `test.png` shows all test execution.
> * `rinkeby1.png` shows `rinkeby` deployment part 1.
> * `rinkeby2.png` shows `rinkeby` deployment part 2.
> * `mint.png` shows `yarn mint` result.
> * `opensea1.png` shows bids for 5 tokens.
> * `opensea2.png` shows 5 tokens sold.

## First steps

You should have installed `node` and `npm`. Then, run on the root directory `yarn`.

## Testing

To launch the test, open two terminals.

1. First, run `yarn ganache` on the first one.
    * Check the ports and modify the `truffle.js` file if needed.
2. Then, run `yarn test:full` on the second one to test all the contracts.

## Deployment to Rinkeby

I've deployed the contracts to `rinkeby` using `yarn rinkeby` or `yarn rinkeby:full` to compile and migrate. Check `rinkeby1.png` and `rinkeby2.png` screenshots.

> Rinkeby deployment data:
> - Account: `0xeC7a8c3815F30548B2F401EaCD3A3e0835507EbC`
> - **`Verifier`**
>   - Transaction hash: `0xe3ceace25383a341a5524bce36c2d86df250f6e43cd80cdd82bb5cd3f9f90cfc`
>   - Contract address: `0x8c0b04590C1Bb9C90e6929dD9b075c3Fa9063237`
> - **`SolnSquareVerifier`**
>   - Transaction hash: `0xd0093bf99a3b9595aa1865b63f4d37a03a8449fe788cf840145f8176384cf0e5`
>   - Contract address: `0xa3cCDD61cc5c5E2e9F4e7802C2D20df0799aF093`

## Mint token

I've mint 10 tokens using `yarn mint`, here is the result (you can check `mint.png` also) :

````
10 tokens minted, here are the results:
        Transaction: 0x3970b6e72fe14d4a0b20f6036fc4997a5b4b2fcae29b926a7761a919bb1e0d22
        Transaction: 0x9d0958a31253df519bf0af5e6f89cfdc88ca42235450b51376359da8c65b6a3a
        Transaction: 0x6be72280b75545c6866720d55c28046276cb9c85656c1a3d1b0cd4d28b10d2ec
        Transaction: 0xfd86a7685be752ef6b9d8ee1c4e829cfc3e9834986561a40ee81e37a7e44faf7
        Transaction: 0xe094f24ac51001fa382c13995fb4083a879e58f6e801917b9ce612d93a241ebf
        Transaction: 0x47e623274d8a3225ecffd8f8bc1e7fd44db2477333a3e053aedf941420a6c1de
        Transaction: 0xdc4b12dd87659046865e0d56d42fd95f335a5b726abf5b897e4da99083c96798
        Transaction: 0x97e9d305680b6ccb33ad644f37f29e787a5143b00d6536d8c5d8101ae9b42ed7
        Transaction: 0xf7ea03a81e5ec0470110fad871a5c164ac6be38cf6e7d2f93e2e23b132fe00da
        Transaction: 0x690960565da185dadf0a7b66b764f2fc60adb36625041c514a7115cf17010f57
````

## OpenSea

Contract is available on OpenSea in this link: [https://rinkeby.opensea.io/category/capstonesuperdifferent](https://rinkeby.opensea.io/category/capstonesuperdifferent)

I've minted 5 items with the account `0xeC7a8c3815F30548B2F401EaCD3A3e0835507EbC`:
- [https://rinkeby.opensea.io/assets/0xa3ccdd61cc5c5e2e9f4e7802c2d20df0799af093/1](https://rinkeby.opensea.io/assets/0xa3ccdd61cc5c5e2e9f4e7802c2d20df0799af093/1)
- [https://rinkeby.opensea.io/assets/0xa3ccdd61cc5c5e2e9f4e7802c2d20df0799af093/2](https://rinkeby.opensea.io/assets/0xa3ccdd61cc5c5e2e9f4e7802c2d20df0799af093/2)
- [https://rinkeby.opensea.io/assets/0xa3ccdd61cc5c5e2e9f4e7802c2d20df0799af093/3](https://rinkeby.opensea.io/assets/0xa3ccdd61cc5c5e2e9f4e7802c2d20df0799af093/3)
- [https://rinkeby.opensea.io/assets/0xa3ccdd61cc5c5e2e9f4e7802c2d20df0799af093/4](https://rinkeby.opensea.io/assets/0xa3ccdd61cc5c5e2e9f4e7802c2d20df0799af093/4)
- [https://rinkeby.opensea.io/assets/0xa3ccdd61cc5c5e2e9f4e7802c2d20df0799af093/5](https://rinkeby.opensea.io/assets/0xa3ccdd61cc5c5e2e9f4e7802c2d20df0799af093/5)

Then, I've place a bid for each of 0.1 weth with the account `0x11a3D3DfD973a6e43423B5100E54d9B175088e54`
> NOTE: I've attached a screenshot of this bids, check `opensea1.png`.

Finally, I sold the tokens to from the account `0xeC7a8c3815F30548B2F401EaCD3A3e0835507EbC` to the account `0x11a3D3DfD973a6e43423B5100E54d9B175088e54`. Here are the trasnactions:
- Transaction for #1: [https://rinkeby.etherscan.io/tx/0x56db738f52857246d57e095ae1b0ebda12e87a9ee3d45beb815589a2d4669018](https://rinkeby.etherscan.io/tx/0x56db738f52857246d57e095ae1b0ebda12e87a9ee3d45beb815589a2d4669018)
- Transaction for #2: [https://rinkeby.etherscan.io/tx/0x0a285f955f476fe80944ac2b9e1ca7054acb8838b90744d19cbc859d3d8cec1e](https://rinkeby.etherscan.io/tx/0x0a285f955f476fe80944ac2b9e1ca7054acb8838b90744d19cbc859d3d8cec1e)
- Transaction for #3: [https://rinkeby.etherscan.io/tx/0x62938f226795a1a28768c6f2821b1fd26f5908397231b39e69e7a485da9dc514](https://rinkeby.etherscan.io/tx/0x62938f226795a1a28768c6f2821b1fd26f5908397231b39e69e7a485da9dc514)
- Transaction for #4: [https://rinkeby.etherscan.io/tx/0xa5ec0737c354dfd49458ad41356d0543485b957e33636306dfd735ee7359ff8d](https://rinkeby.etherscan.io/tx/0xa5ec0737c354dfd49458ad41356d0543485b957e33636306dfd735ee7359ff8d)
- Transaction for #5: [https://rinkeby.etherscan.io/tx/0xe16d02cdee231a6dfbdfeb3c3a1cc2e18407bebe8e9a41644e90268af73aaa7f](https://rinkeby.etherscan.io/tx/0xe16d02cdee231a6dfbdfeb3c3a1cc2e18407bebe8e9a41644e90268af73aaa7f)
> NOTE: I've attached a screenshot of the sold transactions, check `opensea2.png`.

Now, the account `0x11a3D3DfD973a6e43423B5100E54d9B175088e54` has the ownership of the tokens, as can be checked here: [https://rinkeby.etherscan.io/address/0x11a3D3DfD973a6e43423B5100E54d9B175088e54#tokentxnsErc721](https://rinkeby.etherscan.io/address/0x11a3D3DfD973a6e43423B5100E54d9B175088e54#tokentxnsErc721)
