// Imports
const HDWalletProvider = require('truffle-hdwallet-provider')
const Web3 = require('web3')
// ... Local imports
const CONTRACT_FILE = require('./build/contracts/SolnSquareVerifier')
const proof = require('./zokrates/code/square/proof')
// ... Read enviroment variables
require('dotenv').config()
// Constant values
const ABI = CONTRACT_FILE.abi
const OWNER_ADDRESS = '0xeC7a8c3815F30548B2F401EaCD3A3e0835507EbC'
const CONTRACT_ADDRESS = '0xa3cCDD61cc5c5E2e9F4e7802C2D20df0799aF093'
const MAX_TOKENS = 10
const GAS_CONTRACT = '1000000'
const GAS_TRANS = 3000000
const BASE = 10
const web3 = new Web3(new HDWalletProvider(process.env.MNEMONIC, `https://rinkeby.infura.io/v3/${process.env.INFURA_API_KEY}`))
// Mint function
const mint = async () => {
  const contract = new web3.eth.Contract(ABI, CONTRACT_ADDRESS, {
    gasLimit: GAS_CONTRACT
  })
  const promises = []
  for (let token = BASE; token < MAX_TOKENS + BASE; token++) {
    promises.push(new Promise((resolve, reject) => {
      contract.methods.mintVerified(
        OWNER_ADDRESS,
        token,
        proof.proof.A, 
        proof.proof.A_p, 
        proof.proof.B, 
        proof.proof.B_p, 
        proof.proof.C, 
        proof.proof.C_p, 
        proof.proof.H, 
        proof.proof.K, 
        proof.input
      ).send({
        from: OWNER_ADDRESS,
        gas: GAS_TRANS
      }, (error, result) => {
        if (error) {
          reject(error)
        } else {
          resolve(result)
        }
      })
    }))
  }
  const results = await Promise.all(promises)
  console.log(`${MAX_TOKENS} tokens minted, here are the results:`)
  for (let result of results) {
    console.log(`\tTransaction: ${result}`)
  }
}
// Execute function
mint()
