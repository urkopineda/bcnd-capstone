pragma solidity ^0.5.2;

contract Ownable {

    address private _owner;

    event OwnershipTransfered(address owner);

    constructor()
    public {
        _owner = msg.sender;
        emit OwnershipTransfered(_owner);
    }

    modifier requireOnlyOwner() {
        require(_owner == msg.sender, "You are not the owner of the contract");
        _;
    }

    function getOwnerAddress()
    public
    view
    returns (address owner) {
        return _owner;
    }

    function transferOwnership(address owner)
    public
    requireOnlyOwner {
        _owner = owner;
        emit OwnershipTransfered(_owner);
    }

}
