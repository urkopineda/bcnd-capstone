pragma solidity ^0.5.2;

import './Ownable.sol';

contract Pausable is Ownable {

    bool private _paused;

    event Paused(address addr);
    event Unpaused(address addr);

    constructor()
    public {
        _paused = false;
    }

    modifier requirePaused() {
        require(_paused == true, "The contract is not paused");
        _;
    }

    modifier requireNotPaused() {
        require(_paused == false, "The contract is paused");
        _;
    }

    function setPaused(bool paused)
    public
    requireOnlyOwner {
      _paused = paused;
      if (_paused) {
        emit Paused(msg.sender);
      } else {
        emit Unpaused(msg.sender);
      }
    }

}
