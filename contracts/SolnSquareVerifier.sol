pragma solidity >=0.4.21 <0.6.0;

import './Verifier.sol';
import './ERC721Mintable.sol';

contract SolnSquareVerifier is CustomERC721Token {

    struct Solutions {
        uint256 tokenId;
        address owner;
    }

    Solutions[] private solutionsArray;

    mapping(bytes32 => Solutions) private solutionsMapping;

    event SolutionAdded(address owner, uint256 index);

    Verifier public verifier;

    constructor (address verAddress) public {
        verifier = Verifier(verAddress);
    }

    function addSolution(uint256 index, address user, bytes32 key) internal {
        Solutions memory sol = Solutions({
          tokenId: index,
          owner: user
        });
        solutionsMapping[key] = sol;
        emit SolutionAdded(user, index);
    }

    function mintVerified(
        address to,
        uint256 tokenId,
        uint[2] memory a,
        uint[2] memory a_p,
        uint[2][2] memory b,
        uint[2] memory b_p,
        uint[2] memory c,
        uint[2] memory c_p,
        uint[2] memory h,
        uint[2] memory k,
        uint[2] memory input
    ) 
    public {
        bytes32 key = keccak256(abi.encodePacked(a, a_p, b, b_p, c, c_p, h, k, input));
        require(solutionsMapping[key].owner == address(0), "Solutions already used.");
        require(verifier.verifyTx(a, a_p, b, b_p, c, c_p, h, k, input), "Unable to verify. Incorrect Proof.");
        addSolution(tokenId, to, key);
        super.mint(to, tokenId);
    }
}